---TEXT RPG---
- Kostka
Hod kostkou od 1 do 20
20 = Super uspěch, př. dvojnásobné poškození nebo perfektní blokování
1 = Neuspěch, př. 100% proražené blokování a minutí utoku

TŘÍDY PROGRAMU:
Player - má všechny různé interakce a statistiky hráče
Game - páteř hry, stará se o načítání a ukládání dat, spouštění soubojů atd.
Enemy1/2/3 - třída nepřátel
Boss1/2/3 - třídá bossů

TŘÍDA PLAYER:
* Rozhoduje o statistikách hráče pomočí enumu "PlayerClass"
* Má metodu "hodit kostkou" pro rozhodování poškození atd.

TŘÍDA GAME:
* Metoda "Play" která je v podstatě hra samotná
* Metoda "Battle", vytvoří nepřítele a spustí souboj
* Metoda "Load/Save" použije steamwriter/reader pro načtení/přečtení dat

todo

Každý boss má nějakou unikátní schopnost