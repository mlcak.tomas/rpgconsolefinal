﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGTEXT2
{
    class Player : ICharacter
    {
        public string Name;
        public string cName;
        public double MaxHP;
        public double HP;
        public double Damage;
        public double Defense;
        public PlayerClass playerClass;
        public int round;
        public int Level; //TODO
        public bool hasPassive = false;
        public bool hasActive = false;
        public Player(string name, PlayerClass classs)
        {
            Name = name;
            playerClass = classs;
            switch (playerClass)
            {
                case (PlayerClass.Thief):
                    cName = "Kapsář";
                    MaxHP = 100;
                    Damage = 30;
                    Defense = 15;
                    break;
                case (PlayerClass.Warrior): //warrior
                    cName = "Panoš";
                    MaxHP = 120;
                    Damage = 20;
                    Defense = 30;
                    break;
                case (PlayerClass.Shield): //defender
                    cName = "Štítonoš";
                    MaxHP = 170;
                    Damage = 15;
                    Defense = 50;
                    break;
            }
            HP = MaxHP;
        }

        public Player Load(PlayerClass clas, int levl, string name)
        {
            Player plyr = new Player(name, clas);
            switch (clas)
            {
                case (PlayerClass.Thief):
                    switch (levl)
                    {
                        case 1:
                            plyr.cName = "Zloděj";
                            plyr.MaxHP = 150;
                            plyr.Damage = 50;
                            plyr.Defense = 25;
                            return plyr;
                        case 2:
                            plyr.cName = "Legendární Zloděj";
                            plyr.MaxHP = 250;
                            plyr.Damage = 75;
                            plyr.Defense = 45;
                            return plyr;
                        default:
                            break;
                    }
                    break;
                case (PlayerClass.Warrior):
                    switch (levl)
                    {
                        case 1:
                            plyr.cName = "Rytíř";
                            plyr.MaxHP = 180;
                            plyr.Damage = 35;
                            plyr.Defense = 50;
                            return plyr;
                        case 2:
                            plyr.cName = "Železný lord";
                            plyr.MaxHP = 300;
                            plyr.Damage = 60;
                            plyr.Defense = 80;
                            return plyr;
                        default:
                            break;
                    }
                    break;
                case (PlayerClass.Shield):
                    switch (levl)
                    {
                        case 1:
                            plyr.cName = "Obránce";
                            plyr.MaxHP = 250;
                            plyr.Damage = 25;
                            plyr.Defense = 120;
                            return plyr;
                        case 2:
                            plyr.cName = "Bastilla";
                            plyr.MaxHP = 400;
                            plyr.Damage = 40;
                            plyr.Defense = 200;
                            return plyr;
                        default:
                            break;
                    }
                    break;
            }
            return null;
        }
        public double Roll(Dice dice)
        {
            double roll = dice.Throw();
            return roll;
        }

        public void WriteStats()
        {
            Console.WriteLine("------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(Name);
            Console.ResetColor();
            Console.WriteLine(cName);
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"HP: {HP}/{MaxHP}");
            Console.ResetColor();
            Console.WriteLine($"DMG: {Damage}");
            Console.WriteLine($"DEF: {Defense}");
        }
    }
    enum PlayerClass
    {
        Thief = 1,
        Warrior = 2,
        Shield = 3
    }
}
