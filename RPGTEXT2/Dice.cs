﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGTEXT2
{
    class Dice
    {
        public double Roll;

        public Dice()
        {

        }

        public double Throw()
        {
            Random r = new Random();
            Roll = r.Next(0,21);
            return Roll;
        }
    }
}
