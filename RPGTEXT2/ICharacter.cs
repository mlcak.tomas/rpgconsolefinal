﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGTEXT2
{
    interface ICharacter
    {
        void WriteStats();
        double Roll(Dice dice);
    }
}
