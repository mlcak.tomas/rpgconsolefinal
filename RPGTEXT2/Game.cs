﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace RPGTEXT2
{
    class Game
    {
        public Game()
        {

        }

        public void Play(Player player)
        {
            Random enemyPicker = new Random();
            bool won = true;
            while (true)
            {
                Enemy1 enemy = new Enemy1(enemyPicker.Next(1, 4));
                won = Battle(player, enemy); //battle vrací bool
                Console.Clear();
                if (won)
                {
                    player.round++;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("VÍTĚZSTVÍ!");
                    Console.ResetColor();
                    Console.WriteLine("Hra byla uložena!");
                    Console.WriteLine($"Goblin boss přijde za {5 - player.round} souboj(e)...");
                    Console.ReadKey();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("HRA U KONCE");
                    Console.WriteLine($"{player.Name} byl zabit {enemy.Name}.");
                    Console.ReadKey();
                }
            }

        }
        public bool Battle(Player player, Enemy1 enemy)
        {
            while (true)
            {
                if (player.HP <= 0)
                {
                    return false;
                }
                else if (enemy.HP <= 0)
                {
                    return true;
                }
                if(player.MaxHP < player.HP) //kontrola životů
                {
                    player.HP = player.MaxHP;
                }
                Console.Clear();
                player.WriteStats();
                enemy.WriteStats();
                Console.WriteLine("X--------X");
                Console.WriteLine("1) Utočit");
                Console.WriteLine("2) Bránit");
                switch (Console.ReadKey(true).Key)
                {
                    case (ConsoleKey.NumPad1):
                        Attack(player, enemy);
                        continue;
                    case (ConsoleKey.NumPad2):
                        Defend(player, enemy);
                        break;
                    default:
                        break;
                }


            }
        }

        public Player Create(Game game)
        {
            Console.Clear();
            Console.WriteLine("Zadej jméno postavy:");
            string name = Console.ReadLine();
            Console.WriteLine("Vyber si třídu:");
            Console.WriteLine("1) Kapsář");
            Console.WriteLine("+ Velice vysoké poškození");
            Console.WriteLine("- Nížké životy");
            Console.WriteLine("- Nízká obrana");
            Console.WriteLine("2) Panoš");
            Console.WriteLine("+ Průměrné poškození");
            Console.WriteLine("+ Průměrné životy");
            Console.WriteLine("+ Průměrná obrana");
            Console.WriteLine("3) Štítonoš");
            Console.WriteLine("+ Vysoké životy");
            Console.WriteLine("+ Vysoká obrana");
            Console.WriteLine("- Nízké poškození");
            while (true)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.NumPad1:
                        Player newPlayer1 = new Player(name, PlayerClass.Thief);
                        game.Save(newPlayer1);
                        return newPlayer1;
                    case ConsoleKey.NumPad2:
                        Player newPlayer2 = new Player(name, PlayerClass.Warrior);
                        game.Save(newPlayer2);
                        return newPlayer2;
                    case ConsoleKey.NumPad3:
                        Player newPlayer3 = new Player(name, PlayerClass.Shield);
                        game.Save(newPlayer3);
                        return newPlayer3;
                    default:
                        break;
                }
            }
            
        }

        public void Save(Player player)
        {
            using (StreamWriter sw = new StreamWriter($"{player.Name}.txt"))
            {
                //FORMÁT:
                //životy;PlayerClass;level
                sw.WriteLine($"{player.HP};{player.playerClass};{player.Level};{player.round}");
            }
        }
        public Player Load(string name)
        {
            using (StreamReader sr = new StreamReader(name))
            {
                //FORMÁT:
                //životy;PlayerClass;level
                string line = sr.ReadLine();
                string[] stuff = line.Split(';');
                PlayerClass clase = new PlayerClass();
                clase = (PlayerClass)Enum.Parse(typeof(PlayerClass), stuff[1]);
                name.Replace(".txt",""); //FUNGUJ PROSIM
                Player toreturn = new Player(name,clase);
                toreturn = toreturn.Load(clase, Int32.Parse(stuff[2]), name);
                return toreturn;
            }
        }
        public void Attack(Player player, Enemy1 enemy)
        {
            Dice dice = new Dice();
            Console.Clear();
            double playerthrow = player.Roll(dice);
            double PlayerDamage = determineDMG(player, playerthrow);
            Console.Write($"{player.Name} hodil ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(playerthrow);
            Console.WriteLine();
            Console.ResetColor();
            Console.WriteLine("------------------------------");
            if (playerthrow == 20)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("KRITICKÝ ZÁSAH!!");
                Console.WriteLine($"{player.Name} rozdrtil {enemy.Name} ({PlayerDamage} DMG)");
                Console.ResetColor();
                Console.WriteLine($"{enemy.Name} seknul {player.Name} za {enemy.Damage}");
                enemy.HP -= PlayerDamage;
                player.HP -= enemy.Damage;

                Console.ReadKey();
                return;
            }
            else if (playerthrow == 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("KRITICKÁ CHYBA");
                Console.WriteLine($"{player.Name} minul");
                Console.ResetColor();
                Console.WriteLine($"{enemy.Name} seknul {player.Name} za {enemy.Damage}");
                player.HP -= enemy.Damage;

                Console.ReadKey();
                return;
            }
            Console.WriteLine($"{player.Name} seknul {enemy.Name} za {PlayerDamage}");
            Console.WriteLine($"{enemy.Name} seknul {player.Name} za {enemy.Damage}");
            enemy.HP -= PlayerDamage;
            player.HP -= enemy.Damage;

            Console.ReadKey();  
            return;
        }
        public void Defend(Player player, Enemy1 enemy)
        {
            Dice dice = new Dice();
            Console.Clear();
            double playerthrow = player.Roll(dice);
            double playerBlock = determineDEF(player, playerthrow);
            Console.Write($"{player.Name} hodil ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(playerthrow);
            Console.WriteLine();
            Console.ResetColor();
            Console.WriteLine("------------------------------");
            if (playerthrow == 20)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("PERFEKTNÍ BLOKOVÁNÍ!");
                Console.WriteLine($"{player.Name} se zotavil za {playerBlock} životů");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{player.Name} odrazil marný útok {enemy.Name} zpět! ({enemy.Damage} DMG)");
                Console.ResetColor();
                player.HP += playerBlock;
                enemy.HP -= enemy.Damage;
                Console.ReadKey();
                return;
            }
            else if (playerthrow == 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("TREFA DO SLABINY!");
                Console.WriteLine($"{player.Name} se zmateně rozhlíží");
                Console.ResetColor();
                Console.WriteLine($"{enemy.Name} udává ránu nepřipravenému {player.Name} za {enemy.Damage * 2}!!");
                player.HP -= enemy.Damage * 2;

                Console.ReadKey();
                return;
            }
            bool puncture = false;
            puncture = determinePunct(player, enemy);
            Console.WriteLine($"{player.Name} se zotavil za {playerBlock} životů");
            if(puncture == true)
            {
                Console.WriteLine("[PRÁSK!!]");
                Console.WriteLine();
                Console.WriteLine($"{enemy.Name} prorazil obranu {player.Name} ({enemy.Damage} DMG)");
                player.HP -= enemy.Damage;
            }
            else
            {
                Console.WriteLine("[cink]");
                Console.WriteLine($"{player.Name} odrazil útok {enemy.Name}");
            }
            player.HP += playerBlock;

            Console.ReadKey();
            return;
        }
        public double determineDMG(Player player, double roll)
        {
            if(roll == 0)
            {
                return 0;
            }
            return roll / 10 * player.Damage;
        }
        public double determineDEF(Player player, double roll)
        {
            if (roll == 0)
            {
                return 0;
            }
            return (roll * player.Defense) / 10;
        }
        public bool determinePunct(Player player, Enemy1 enemy)
        {
            Random r = new Random();
            int chance = 0;
            if (player.Defense >= enemy.Damage)
            {
                chance = r.Next(1, 5); //20% když má hráč vyšší obranu než nepřítelovo poškození
                if (chance == 3)
                {
                    return true; //Proraženo
                }
                else
                {
                    return false;
                }
            }
            else
            {
                chance = r.Next(1, 3); //33% když má hráč nižší obranu než nepřítelovo poškození
                if (chance == 2)
                {
                    return true; //Proraženo
                }
                else
                {
                    return false;
                }
            }
        }
    }
}

