﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGTEXT2
{
    class Enemy1 : ICharacter
    {
        //Goblin
        public string Name;
        public double MaxHP;
        public double HP;
        public double Damage;
        public double Defense;

        public Enemy1(int version)
        {
            switch (version)
            {
                case 1:
                    Name = "Goblin zloděj";
                    MaxHP = 40;
                    HP = MaxHP;
                    Damage = 20;
                    Defense = 0;
                    break;
                case 2:
                    Name = "Goblin sekerník";
                    MaxHP = 50;
                    HP = MaxHP;
                    Damage = 10;
                    Defense = 0;
                    break;
                case 3:
                    Name = "Goblin šampion";
                    MaxHP = 70;
                    HP = MaxHP;
                    Damage = 10;
                    Defense = 0;
                    break;
            }

        }
    public double Roll(Dice dice)
        {
            double roll = dice.Throw();
            return roll;
        }

        public void WriteStats()
        {
            Console.WriteLine("------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(Name);
            Console.ResetColor();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"HP: {HP}/{MaxHP}");
            Console.ResetColor();
            Console.WriteLine($"DMG: {Damage}");
            Console.WriteLine($"DEF: {Defense}");
            Console.WriteLine("------------------------------------------------");
        }
    }
}
