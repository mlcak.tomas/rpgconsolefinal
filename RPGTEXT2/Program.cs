﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPGTEXT2
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Game game = new Game();
            Dice dice = new Dice();
            while (true)
            {
                Player player;
                Console.WriteLine("textRPG");
                Console.WriteLine("1) Vytvořit novou postavu");
                Console.WriteLine("2) Načíst postavu");
                Console.WriteLine("3) Konec hry");
                while (true)
                {
                    switch (Console.ReadKey(true).Key)
                    {
                        case (ConsoleKey.NumPad1):
                            player = game.Create(game);
                            game.Play(player);
                            break;
                        case (ConsoleKey.NumPad2):
                            using (OpenFileDialog openfiledialog = new OpenFileDialog())
                            {
                                openfiledialog.Filter = "text|*.txt|all|*.*";
                                if (openfiledialog.ShowDialog() == DialogResult.OK)
                                {
                                    string name = openfiledialog.SafeFileName;
                                    player = game.Load(name);
                                    game.Play(player);
                                }
                            }
                            break;
                        case (ConsoleKey.NumPad3):
                            System.Environment.Exit(1);
                            break;
                        default:
                            break;
                    }

                }
            }


        }
    }
}
